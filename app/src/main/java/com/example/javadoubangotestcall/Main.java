package com.example.javadoubangotestcall;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.doubango.ngn.NgnEngine;
import org.doubango.ngn.events.NgnEventArgs;
import org.doubango.ngn.events.NgnInviteEventArgs;
import org.doubango.ngn.events.NgnRegistrationEventArgs;
import org.doubango.ngn.media.NgnMediaType;
import org.doubango.ngn.services.INgnConfigurationService;
import org.doubango.ngn.services.INgnSipService;
import org.doubango.ngn.sip.NgnAVSession;
import org.doubango.ngn.utils.NgnConfigurationEntry;
import org.doubango.ngn.utils.NgnUriUtils;


public class Main extends ActivityGroup {
        private ListView mListView;
        private TextView mTvLog;
        private MainAdapter mAdapter;

        public static final int ACTION_NONE = 0;
        public static final int ACTION_RESTORE_LAST_STATE = 1;
        public static final int ACTION_SHOW_AVSCREEN = 2;
        public static final int ACTION_SHOW_CONTSHARE_SCREEN = 3;

        private BroadcastReceiver mSipBroadCastRecv;
        private PowerManager.WakeLock mWakeLock;

        private final NgnEngine mNgnEngine;
        private final Engine mEngine;
        private final INgnConfigurationService mConfigurationService;
        private final INgnSipService mSipService;

        private final static String SIP_DOMAIN = "sip2sip.info";
        private final static String SIP_USERNAME = "2233575381@sip2sip.info";
        private final static String SIP_PASSWORD = "gfhjkm1";
        private final static String SIP_SERVER_HOST = "proxy.sipthor.net";
        private final static int SIP_SERVER_PORT = 5060;

        public final static String EXTRAT_SIP_SESSION_ID = "SipSession";
        public final static String TAG = Main.class.getCanonicalName();

        static final MainListViewItem[] sMainListViewItems = new MainListViewItem[]{
                new MainListViewItem("001", "2233575137@sip2sip.info"),
        };

        public Main(){
            mNgnEngine = NgnEngine.getInstance();
            mEngine = Engine.getInstance();
            mConfigurationService = mNgnEngine.getConfigurationService();
            mSipService = mNgnEngine.getSipService();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.main);


            final PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if(powerManager != null && mWakeLock == null){
                mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

            }

            mTvLog = findViewById(R.id.main_textView_log);

            mAdapter = new MainAdapter(this);
            mListView = findViewById(R.id.main_listView);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });
            mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    return false;
                }
            });

            mTvLog.setText("onCreate()");

            // Listen for registration events
            mSipBroadCastRecv = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final String action = intent.getAction();

                    // Registration Event
                    if(NgnRegistrationEventArgs.ACTION_REGISTRATION_EVENT.equals(action)){
                        NgnRegistrationEventArgs args = intent.getParcelableExtra(NgnEventArgs.EXTRA_EMBEDDED);
                        if(args == null){
                            mTvLog.setText("Invalid event args");
                            return;
                        }
                        switch(args.getEventType()){
                            case REGISTRATION_NOK:
                                mTvLog.setText("Failed to register :(");
                                break;
                            case UNREGISTRATION_OK:
                                mTvLog.setText("You are now unregistered :)");
                                break;
                            case REGISTRATION_OK:
                                mTvLog.setText("You are now registered :)");
                                break;
                            case REGISTRATION_INPROGRESS:
                                mTvLog.setText("Trying to register...");
                                break;
                            case UNREGISTRATION_INPROGRESS:
                                mTvLog.setText("Trying to unregister...");
                                break;
                            case UNREGISTRATION_NOK:
                                mTvLog.setText("Failed to unregister :(");
                                break;
                        }
                        mAdapter.refresh();
                    }
                    else if(NgnInviteEventArgs.ACTION_INVITE_EVENT.equals(action)){
                        NgnInviteEventArgs args = intent.getParcelableExtra(NgnEventArgs.EXTRA_EMBEDDED);
                        if(args == null){
                            Log.e(TAG, "Invalid event args");
                            return;
                        }

                        final NgnMediaType mediaType = args.getMediaType();

                        switch(args.getEventType()){
                            case TERMWAIT:
                            case TERMINATED:
                                if(NgnMediaType.isAudioVideoType(mediaType)){
                                    mEngine.refreshAVCallNotif(R.drawable.phone_call_25);
                                    mEngine.getSoundService().stopRingBackTone();
                                    mEngine.getSoundService().stopRingTone();
                                }
                                break;

                            case INCOMING:
                                if(NgnMediaType.isAudioVideoType(mediaType)){
                                    final NgnAVSession avSession = NgnAVSession.getSession(args.getSessionId());
                                    if(avSession != null){
                                        mEngine.showAVCallNotif(R.drawable.phone_call_25, getString(R.string.string_call_incoming));
                                        ScreenAV.receiveCall(avSession);
                                        if(mWakeLock != null && !mWakeLock.isHeld()){
                                            mWakeLock.acquire(10);
                                        }
                                        mEngine.getSoundService().startRingTone();
                                    }
                                    else{
                                        Log.e(TAG, String.format("Failed to find session with id=%ld", args.getSessionId()));
                                    }
                                }
                                break;

                            case INPROGRESS:
                                if(NgnMediaType.isAudioVideoType(mediaType)){
                                    mEngine.showAVCallNotif(R.drawable.phone_call_25, getString(R.string.string_call_outgoing));
                                }
                                break;

                            case RINGING:
                                if(NgnMediaType.isAudioVideoType(mediaType)){
                                    mEngine.getSoundService().startRingBackTone();
                                }
                                break;

                            case CONNECTED:
                            case EARLY_MEDIA:
                                if(NgnMediaType.isAudioVideoType(mediaType)){
                                    mEngine.showAVCallNotif(R.drawable.phone_call_25, getString(R.string.string_incall));
                                    mEngine.getSoundService().stopRingBackTone();
                                    mEngine.getSoundService().stopRingTone();
                                }
                                break;
                            default: break;
                        }
                    }

                }
            };
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(NgnRegistrationEventArgs.ACTION_REGISTRATION_EVENT);
            intentFilter.addAction(NgnInviteEventArgs.ACTION_INVITE_EVENT);
            registerReceiver(mSipBroadCastRecv, intentFilter);
        }


        @Override
        protected void onDestroy() {
            // Stops the engine
            if(mNgnEngine.isStarted()){
                mNgnEngine.stop();
            }
            // release the listener
            if (mSipBroadCastRecv != null) {
                unregisterReceiver(mSipBroadCastRecv);
                mSipBroadCastRecv = null;
            }
            super.onDestroy();
        }

        @Override
        protected void onResume() {
            super.onResume();
            // Starts the engine
            if(!mNgnEngine.isStarted()){
                if(mNgnEngine.start()){
                    mTvLog.setText("Engine started :)");
                }
                else{
                    mTvLog.setText("Failed to start the engine :(");
                }
            }
            // Register
            if(mNgnEngine.isStarted()){
                if(!mSipService.isRegistered()){
                    // Set credentials
                    mConfigurationService.putString(NgnConfigurationEntry.IDENTITY_IMPI, SIP_USERNAME);
                    mConfigurationService.putString(NgnConfigurationEntry.IDENTITY_IMPU, String.format("sip:%s@%s", SIP_USERNAME, SIP_DOMAIN));
                    mConfigurationService.putString(NgnConfigurationEntry.IDENTITY_PASSWORD, SIP_PASSWORD);
                    mConfigurationService.putString(NgnConfigurationEntry.NETWORK_PCSCF_HOST, SIP_SERVER_HOST);
                    mConfigurationService.putInt(NgnConfigurationEntry.NETWORK_PCSCF_PORT, SIP_SERVER_PORT);
                    mConfigurationService.putString(NgnConfigurationEntry.NETWORK_REALM, SIP_DOMAIN);
                    // VERY IMPORTANT: Commit changes
                    mConfigurationService.commit();
                    // register (log in)
                    mSipService.register(this);
                }
            }
        }

        boolean makeVoiceCall(String phoneNumber){
            final String validUri = NgnUriUtils.makeValidSipUri(String.format("sip:%s@%s", phoneNumber, SIP_DOMAIN));
            if(validUri == null){
                mTvLog.setText("failed to normalize sip uri '" + phoneNumber + "'");
                return false;
            }
            NgnAVSession avSession = NgnAVSession.createOutgoingSession(mSipService.getSipStack(), NgnMediaType.Audio);

            Intent i = new Intent();
            i.setClass(this, CallScreen.class);
            i.putExtra(EXTRAT_SIP_SESSION_ID, avSession.getId());
            startActivity(i);

            return avSession.makeCall(validUri);
        }

        //
        //	MainListViewItem
        //
        static class MainListViewItem{
            private final String mDescription;
            private final String mPhoneNumber;

            MainListViewItem(String description, String phoneNumer){
                mDescription = description;
                mPhoneNumber = phoneNumer;
            }

            String getDescription(){
                return mDescription;
            }

            String getPhoneNumber(){
                return mPhoneNumber;
            }
        }

        //
        //
        //
        static class MainAdapter extends BaseAdapter {
            final LayoutInflater mInflater;
            final Main mMain;
            MainAdapter(Main main){
                super();
                mMain = main;
                mInflater = LayoutInflater.from(main);
            }

            void refresh(){
                notifyDataSetChanged();
            }

            @Override
            public int getCount() {
                return sMainListViewItems.length;
            }

            @Override
            public Object getItem(int position) {
                return sMainListViewItems[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = convertView;
                final MainListViewItem item = (MainListViewItem)getItem(position);
                if(view == null){
                    view = mInflater.inflate(R.layout.main_item, null);
                }
                ((TextView)view.findViewById(R.id.main_item_textView1_description)).setText(item.getDescription());
                final Button button = (Button)view.findViewById(R.id.main_item_button_call);
                button.setEnabled(mMain.mSipService.isRegistered());
                button.setTag(item.getPhoneNumber());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMain.makeVoiceCall(v.getTag().toString());
                    }
                });
                return view;
            }
        }
    }

