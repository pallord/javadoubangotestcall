package com.example.javadoubangotestcall;

import android.view.Menu;

public interface IBaseScreen {
    String getId();
    BaseScreen.SCREEN_TYPE getType();
    boolean hasMenu();
    boolean hasBack();
    boolean back();
    boolean createOptionsMenu(Menu menu);
}
