package com.example.javadoubangotestcall;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import org.doubango.ngn.NgnEngine;
import org.doubango.ngn.sip.NgnAVSession;

public class Engine extends NgnEngine {
    private final static String TAG = Engine.class.getCanonicalName();

    private static final String CONTENT_TITLE = "IMSDroid";

    private static final int NOTIF_AVCALL_ID = 19833892;
    private static final int NOTIF_SMS_ID = 19833893;
    private static final int NOTIF_APP_ID = 19833894;
    private static final int NOTIF_CONTSHARE_ID = 19833895;
    private static final int NOTIF_CHAT_ID = 19833896;
    private static Engine sInstance;

    private IScreenService mScreenService;

    public static Engine getInstance(){
        if(sInstance == null){
            sInstance = new Engine();
        }
        return sInstance;
    }

    public Engine(){
        super();
    }

    @Override
    public boolean start() {
        return super.start();
    }

    @Override
    public boolean stop() {
        return super.stop();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showNotification(int notifId, int drawableId, String tickerText) {
        if(!mStarted){
            return;
        }
        Notification notification = null;
        Intent intent = new Intent(App.getContext(), Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP  | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(App.getContext(), notifId/*requestCode*/, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set the info for the views that show in the notification panel.
        //notification.setLatestEventInfo(IMSDroid.getContext(), CONTENT_TITLE, tickerText, contentIntent);

            // Use new API
            Notification.Builder builder = new Notification.Builder(App.getContext())
                    .setContentIntent(contentIntent)
                    .setSmallIcon(drawableId)
                    .setContentText(tickerText)
                    .setContentTitle(CONTENT_TITLE);
            notification = builder.build();


        switch(notifId){
            case NOTIF_APP_ID:
                notification.flags |= Notification.FLAG_ONGOING_EVENT;
                intent.putExtra("notif-type", "reg");
                break;

            case NOTIF_CONTSHARE_ID:
                intent.putExtra("action", Main.ACTION_SHOW_CONTSHARE_SCREEN);
                notification.defaults |= Notification.DEFAULT_SOUND;
                break;

            case NOTIF_AVCALL_ID:
                tickerText = String.format("%s (%d)", tickerText, NgnAVSession.getSize());
                intent.putExtra("action", Main.ACTION_SHOW_AVSCREEN);
                break;

            default:

                break;
        }



        // Send the notification.
        // We use a layout id because it is a unique number.  We use it later to cancel.
        mNotifManager.notify(notifId, notification);
    }

    public void showAppNotif(int drawableId, String tickerText){
        Log.d(TAG, "showAppNotif");
        showNotification(NOTIF_APP_ID, drawableId, tickerText);
    }

    public void showAVCallNotif(int drawableId, String tickerText){
        showNotification(NOTIF_AVCALL_ID, drawableId, tickerText);
    }

    public void cancelAVCallNotif(){
        if(!NgnAVSession.hasActiveSession()){
            mNotifManager.cancel(NOTIF_AVCALL_ID);
        }
    }

    public void refreshAVCallNotif(int drawableId){
        if(!NgnAVSession.hasActiveSession()){
            mNotifManager.cancel(NOTIF_AVCALL_ID);
        }
        else{
            showNotification(NOTIF_AVCALL_ID, drawableId, "In Call");
        }
    }

    public IScreenService getScreenService(){
        if(mScreenService == null){
            mScreenService = new ScreenService();
        }
        return mScreenService;
    }

   /* @Override
    public Class<? extends NgnNativeService> getNativeServiceClass(){
        return NativeService.class;
    }*/
}

