package com.example.javadoubangotestcall;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityGroup;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.doubango.ngn.sip.NgnAVSession;
import org.doubango.ngn.utils.NgnPredicate;
import org.doubango.ngn.utils.NgnStringUtils;

import static com.example.javadoubangotestcall.Main.ACTION_RESTORE_LAST_STATE;
import static com.example.javadoubangotestcall.Main.ACTION_SHOW_AVSCREEN;

public class InputCallScreen extends ActivityGroup {

    private Handler mHanler;
    private Engine mEngine;
    private IScreenService mScreenService;
    private final static String TAG = InputCallScreen.class.getCanonicalName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_call_screen);

        mEngine = Engine.getInstance();
        mEngine.setMainActivity(this);
        mScreenService = Engine.getInstance().getScreenService();

        mHanler = new Handler();
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        Bundle bundle = savedInstanceState;
        if(bundle == null){
            Intent intent = getIntent();
            bundle = intent == null ? null : intent.getExtras();
        }
        if(bundle != null && bundle.getInt("action", Main.ACTION_NONE) != Main.ACTION_NONE){
            handleAction(bundle);
        }
    }
    private void handleAction(Bundle bundle){
        final String id;
        switch(bundle.getInt("action", Main.ACTION_NONE)){
            // Default or ACTION_RESTORE_LAST_STATE
            default:
            case ACTION_RESTORE_LAST_STATE:
                id = bundle.getString("screen-id");
                final String screenTypeStr = bundle.getString("screen-type");
                final BaseScreen.SCREEN_TYPE screenType = NgnStringUtils.isNullOrEmpty(screenTypeStr) ? BaseScreen.SCREEN_TYPE.HOME_T :
                        BaseScreen.SCREEN_TYPE.valueOf(screenTypeStr);
                switch(screenType){
                    case AV_T:
                        mScreenService.show(ScreenAV.class, id);
                        break;
                    default:
                        if(!mScreenService.show(id)){
                            mScreenService.show(Main.class);
                        }
                        break;
                }
                break;

            // Show Audio/Video Calls
            case ACTION_SHOW_AVSCREEN:
                Log.d(TAG, "Main.ACTION_SHOW_AVSCREEN");

                final int activeSessionsCount = NgnAVSession.getSize(new NgnPredicate<NgnAVSession>() {
                    @Override
                    public boolean apply(NgnAVSession session) {
                        return session != null && session.isActive();
                    }
                });
                if(activeSessionsCount > 1){
                    //mScreenService.show(ScreenAVQueue.class);
                }
                else{
                    NgnAVSession avSession = NgnAVSession.getSession(new NgnPredicate<NgnAVSession>() {
                        @Override
                        public boolean apply(NgnAVSession session) {
                            return session != null && session.isActive() && !session.isLocalHeld() && !session.isRemoteHeld();
                        }
                    });
                    if(avSession == null){
                        avSession = NgnAVSession.getSession(new NgnPredicate<NgnAVSession>() {
                            @Override
                            public boolean apply(NgnAVSession session) {
                                return session != null && session.isActive();
                            }
                        });
                    }
                    if(avSession != null){
                        if(!mScreenService.show(ScreenAV.class, Long.toString(avSession.getId()))){
                            mScreenService.show(Main.class);
                        }
                    }
                    else{
                        Log.e(TAG,"Failed to find associated audio/video session");
                        mScreenService.show(Main.class);
                        mEngine.refreshAVCallNotif(R.drawable.phone_call_25);
                    }
                }
                break;
        }
    }
}
